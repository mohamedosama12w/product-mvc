<?php

namespace App\Core;

use App\Config\BaseConfig;
use PDO;


class DB
{
    private $db;
    private $config;

    public function connect()
    {
        $this->config = new BaseConfig();
        $this->db = new PDO($this->config->getDsn(), $this->config->getUser(), $this->config->getPass(), $this->config->getOptions());
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if (!$this->db) {
            die("Connection Error : ");
        }
        return $this->db;
    }
}
