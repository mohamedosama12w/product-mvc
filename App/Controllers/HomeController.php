<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Models\ProductsModel;

class HomeController extends Controller
{
    private $model;

    public function __construct()
    {
        $this->model = new ProductsModel();
    }

    public function index()
    {
        $products = $this->model->getProducts();
        for ($i = 0; $i < count($products); $i++) {
            $product_type = 'App\\Models\\'.$products[$i]['type'];
            $type = new $product_type($products[$i]['specifications']);
            $products[$i]['value'] = $type->getProductSpecifications();
        }
        $data['products'] = $products;
        return $this->view('products/index', $data);
    }
}
