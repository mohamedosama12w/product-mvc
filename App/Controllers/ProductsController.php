<?php

namespace App\Controllers;

use App\Config\Validation;
use App\Core\Controller;
use App\Models\ProductsModel;

class ProductsController extends Controller
{
    private $model;

    public function __construct()
    {
        $this->model = new ProductsModel();
    }


    public function index( $data = null )
    {
        $products = $this->model->getProducts();
        for ($i = 0; $i < count($products); $i++) {
            $displayed_type = 'App\\Models\\'.$products[$i]['type'];
            $type = new $displayed_type($products[$i]['specifications']);
            $products[$i]['value'] = $type->getProductSpecifications();
        }
        $data['products'] = $products;
        return $this->view('products/index', $data);
    }


    public function add()
    {
        return $this->view('products/add');
    }

    public function store()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $rules = [
                'sku' => ['required','string','min:2','max:50','unique:products'],
                'name' => ['required','string','min:2','max:50'],
                'price' => ['required','number'],
                'type' => ['required','in:DVD,Book,Furniture'],
                'size' => ['required_if:type = DVD','number'],
                'weight' => ['required_if:type = Book','number'],
                'length' => ['required_if:type = Furniture','number'],
                'width' => ['required_if:type = Furniture','number'],
                'height' => ['required_if:type = Furniture','number'],
            ];

            $validation = new Validation($_POST,$rules);

            $errors = $validation->validate();

            $validated_fields = $validation->getFormFields();

            if (count($errors) == 0) {

                $requested_type = 'App\\Models\\'.$_POST['type'];

                $type = new $requested_type();

                $this->model->setSku($_POST['sku']);
                $this->model->setName($_POST['name']);
                $this->model->setPrice($_POST['price']);
                $this->model->setType($_POST['type']);
                $this->model->setSpecifications($type->formProductSpecifications($_POST));

                if($this->model->insertProducts()) {
                    echo json_encode([
                        'msg' => 'Data Added Successfully',
                    ]);
                } else {
                    echo json_encode([
                        'msg' => 'error',
                    ]);
                    http_response_code(402);
                }
            } else {
                echo json_encode([
                    'validated_fields' => $validated_fields,
                    'errors' => $errors,
                ]);
                http_response_code(402);
            }
        }
    }

    public function delete()
    {
        if (isset($_POST['product'])) {
            $deleted_products = $_POST['product'];
            foreach ($deleted_products as $product) {
                $this->model->setId($product);
                $this->model->deleteProduct();
            }
           $this->index();
        } else {
            $data['error'] = "you must choose products to delete";
            $this->index($data);
        }
    }
}
