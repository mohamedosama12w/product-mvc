<?php
include(VIEWS.'includes'.DS.'header.php');
?>

<div class="jumbotron text-center">
  <h1 class="display-4 bg-danger py-3 my-5 text-white">Error Occurred ! :( </h1>
  <a class="btn btn-primary btn-lg" href="/Products" role="button">Back To Home</a>
</div>

<?php  include(VIEWS.'includes'.DS.'footer.php'); ?>
