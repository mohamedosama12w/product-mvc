<?php

include(VIEWS.'includes'.DS.'header.php');


?>

<?php if (isset($error)): ?>
    <h6 class="alert alert-danger text-center"><?php  echo $error; ?></h6>
<?php endif; ?>

<section class="team section-padding">
    <div class="container">
        <div class="row">
            <div class="section-head col-sm-12">
                <div class="row">
                    <div class="col-lg-6">
                        <h4>
                            Product List
                        </h4>
                    </div>
                    <div class="col-lg-6 text-right">
                        <button class="btn btn-primary" onclick="window.location.href ='/Products/add'">ADD</button>
                        <button class="btn btn-danger" id="delete-product-btn" onclick="event.preventDefault();
                              document.getElementById('delete-product-form').submit();">MASS DELETE</button>
                    </div>
                </div>
                <hr class="hr-style">
            </div>
            <div class="pt-80 pb-50">
                <form method="POST" id="delete-product-form" action="/Products/delete">
                    <div class="row">
                        <?php foreach ($products as $product) { ?>
                            <div class="col-lg-3">
                                <div class="card text-center" style="width: 15rem;height: 10rem;margin: 0 250px 40px 0;">
                                    <input class="delete-checkbox" type="checkbox" value="<?php echo $product['id'] ?>" required name="product[]">
                                    <div class="card-body">
                                        <p class="card-text product-info"><?php echo $product['sku'] ?></p>
                                        <p class="card-text product-info"><?php echo $product['name'] ?></p>
                                        <p class="card-text product-info"><?php echo $product['price'].' $' ?></p>
                                        <p class="card-text product-info"><?php echo $product['value'] ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php  include(VIEWS.'includes'.DS.'footer.php'); ?>
