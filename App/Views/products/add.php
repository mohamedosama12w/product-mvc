<?php

include(VIEWS.'includes'.DS.'header.php');

?>

<section class="team section-padding">
    <div class="container">
        <div class="row">
            <div class="section-head col-sm-12">
                <div class="row">
                    <div class="col-lg-6">
                        <h4>
                            Product Add
                        </h4>
                    </div>
                    <div class="col-lg-6 text-right">
                        <button class="btn btn-primary" id="save_product">Save</button>
                        <button class="btn btn-secondary" onclick="window.location.href ='/Products'">Cancel</button>
                    </div>
                </div>
                <hr class="hr-style">
            </div>
            <div class="pt-80 pb-50">
                <form class="add-product" method="POST" id="product_form" action="/Products/store">
                    <div class="form-group">
                        <label for="name">SKU</label>
                        <input type="text" required name="sku" id="sku" class="form-field">
                        <span class="is-invalid">
                            <strong id="error-message-sku"></strong>
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" required name="name" id="name" class="form-field">
                        <span class="is-invalid">
                            <strong id="error-message-name"></strong>
                        </span>
                    </div>

                    <div class="form-group">
                        <label for="price">Price ($)</label>
                        <input type="number" required name="price" min="0" id="price" class="form-field">
                        <span class="is-invalid">
                            <strong id="error-message-price"></strong>
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="switcher">Type Switcher</label>
                        <select class="form-field" name="type" id="productType" required>
                            <option value="" disabled selected>type switcher</option>
                            <option value="DVD">DVD</option>
                            <option value="Book">Book</option>
                            <option value="Furniture">Furniture</option>
                        </select>
                        <span class="is-invalid">
                            <strong id="error-message-type"></strong>
                        </span>
                    </div>

                    <div id="switcher-form">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php  include(VIEWS.'includes'.DS.'footer.php'); ?>
