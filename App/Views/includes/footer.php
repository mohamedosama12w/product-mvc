<footer class="text-center bottom">
    <div class="container">
        <hr class="hr-style">
        <p>Scandiweb Test Assignment</p>
    </div>
</footer>

<script src="<?php echo JS_PATH?>jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo JS_PATH?>bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        $('#save_product').on('click',function(){
            $.ajax({
                url: $('#product_form').attr('action'),
                type: 'POST',
                data: new FormData(document.getElementById("product_form")),
                cache: false,
                processData: false,
                contentType: false,
                success: function (data) {
                    window.location.href = '/Products';
                },
                error: function (data) {
                    console.log(data.responseText)
                    var obj = JSON.parse(data.responseText);
                    var arr = obj.validated_fields;
                    for (var field in obj.errors) {
                        if(arr.includes(field)){
                            var myIndex = arr.indexOf(field);
                            if (myIndex !== -1) {
                                arr.splice(myIndex, 1);
                            }
                        }
                        $('#error-message-'+field).text(obj.errors[field][0]);
                        $('#error-message-'+field).css('display','block');
                    }

                    for (var i = 0; i < arr.length; i++) {
                        $('#error-message-'+arr[i]).css('display','none');
                    }
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('select[name="type"]').on('change',function(){
            var value = $(this).val();
            if (value == "DVD") {
               data = `
                        <div class="form-group">
                            <label for="size">Size (MB)</label>
                            <input type="number" min="0" required class="form-field" name="size" id="size">
                            <span class="is-invalid">
                                <strong id="error-message-size"></strong>
                            </span>
                        </div>
                        <span class="description">Please, provide disk space in MB</span>
               `;
            }else if(value == "Book"){
                data = `
                        <div class="form-group">
                            <label for="weight">Weight (KG)</label>
                            <input type="number" min="0" required class="form-field" name="weight" id="weight">
                            <span class="is-invalid">
                                <strong id="error-message-weight"></strong>
                            </span>
                        </div>
                        <span class="description">Please, provide book weight in KG</span>
                `;
            }
            else{
                data = `
                        <div class="form-group">
                            <label for="qty">Height (CM)</label>
                            <input type="number" min="0" required class="form-field" name="height" id="height">
                            <span class="is-invalid">
                                <strong id="error-message-height"></strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="qty">Width (CM)</label>
                            <input type="number" min="0" required class="form-field" name="width" id="width">
                            <span class="is-invalid">
                                <strong id="error-message-width"></strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="qty">Length (CM)</label>
                            <input type="number" min="0" required class="form-field" name="length" id="length">
                            <span class="is-invalid">
                                <strong id="error-message-length"></strong>
                            </span>
                        </div>
                        <span class="description">Please, provide dimensions in HxWxL format</span>
                        `;
            }

            $('#switcher-form').html(data);
        });
    });
</script>
</body>
</html>
