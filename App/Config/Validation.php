<?php

namespace App\Config;

 use App\Core\DB;
 use Exception;

 class Validation
{
	 private $request;
	 private $form_fields = [];
	 private $rules;
	 private $errormsg;
	 private $errors = [];
     private $db;


	 public function __construct($request, $rules, $errormsg = null)
     {
		 $this->request = $request;
		 $this->rules = $rules;
		 $this->errormsg = $errormsg;
         $this->db = new DB();
	 }

	public function validate()
    {
		foreach ( $this->rules as $key => $value ) {
			if (array_key_exists($key, $this->request)) {
			    array_push($this->form_fields,$key);
				foreach ( $value as $val ) {
                    $this->loadValidationFunctions($key, $val);
				}
			} else {
                array_push($this->form_fields,$key);
                $this->handleMistypedValidationFields($key);
			}
		}
		return $this->errors;
	}


     public function getFormFields()
     {
         return $this->form_fields;
     }

     public function setFormFields($form_fields)
     {
         $this->form_fields = $form_fields;
     }


     private function loadValidationFunctions($key, $value)
     {
         $rule_array = explode(":",$value);
         $rule = $rule_array[0];
         $rule_value = count($rule_array) > 1 ? $rule_array[1] : null;
         if ( $this->errormsg != null ) {
             $index1 = $key . '.' . $rule;
             $index2 = $rule;
             if ( array_key_exists($index1, $this->errormsg)){
                 $this->$rule($key, $this->request[$key], $rule_value, $this->errormsg[$index1]);
             }
             elseif (array_key_exists($index2, $this->errormsg)){
                 $this->$rule($key, $this->request[$key], $rule_value, $this->errormsg[$index2]);
             }
             else {
                 $this->$rule($key, $this->request[$key], $rule_value);
             }
         } else {
             $this->$rule($key, $this->request[$key], $rule_value, null, $this->request);
         }
     }

     private function handleMistypedValidationFields($key)
     {
         if ($key == "type") {
             $this->errors[$key][] = "Please, choose product type";
         } elseif (array_key_exists('type', $this->request)) {
             if (class_exists("App\\Models\\".$this->request['type'])) {
                 $requested_type = 'App\\Models\\'.$this->request['type'];
                 $type = new $requested_type();
                 $type->addProductProperties();
                 if (in_array($key,$type->getProductProperties())) {
                     $this->errors[$key][] = "Please, submit required data";
                 }
             } else {
                 $this->errors['type'][] = "you must choose from the listed types only";
             }
         } else {
             $this->errors[$key][] = "Please, submit required data";
         }
     }


	private function required($inputname, $inputvalue, $rulevalue = null, $errormsg = null, $request = null)
    {
		$errormsg = $errormsg == null ? "Please, submit required data" : $errormsg;
	 	if ($inputvalue == null) {
			$this->errors[$inputname][] = $errormsg;
		}
	}

     private function required_if($inputname, $inputvalue, $rulevalue = null, $errormsg = null, $request = null)
     {
         $errormsg = $errormsg == null ? "Please, submit required data" : $errormsg;
         if ($rulevalue != null) {
             $fieldname = explode( ' = ',$rulevalue)[0];
             $fieldvalue = explode(' = ',$rulevalue)[1];
             if (array_key_exists($fieldname, $request)) {
                 if ($request[$fieldname] == $fieldvalue) {
                     if ($inputvalue == null) {
                         $this->errors[$inputname][] = $errormsg;
                     }
                 }
             }
         } else {
             throw new Exception('you must indicate the field name');
         }
     }

	 private function number($inputname, $inputvalue = null, $rulevalue = null, $errormsg = null,  $request = null)
     {
		 $errormsg = $errormsg == null ? "field is not a valid number" : $errormsg;
		 $pattern = '/^(?!0*[.,]0*$|[.,]0*$|0*$)\d+[,.]?\d{0,2}$/';
		 if ($inputvalue != null) {
             if (!preg_match($pattern, $inputvalue)) {
                 $this->errors[$inputname][] = $errormsg;
             }
         }
	 }

     private function string($inputname, $inputvalue = null, $rulevalue = null, $errormsg = null, $request = null)
     {
         $errormsg = $errormsg == null ? "field is not a valid string" : $errormsg;
         $pattern = '/([%\$#\*<>]+)/';
         if ($inputvalue != null) {
             if (preg_match($pattern, $inputvalue)) {
                 $this->errors[$inputname][] = $errormsg;
             }
         }
     }

     private function unique($inputname, $inputvalue = null, $rulevalue = null, $errormsg = null, $request = null)
     {
         $errormsg = $errormsg == null ? "This value has already been taken." : $errormsg;

         $stmt = $this->db->connect()->prepare("SELECT * FROM `$rulevalue` where `$inputname`= ?");
         $stmt->execute([$inputvalue]);

         $result = $stmt->fetchColumn();
         if ($rulevalue != null) {
             if ($result) {
                 $this->errors[$inputname][] = $errormsg;
             }
         } else {
             throw new Exception('you must indicate the table name');
         }
     }


     private function min($inputname, $inputvalue, $rulevalue = null, $errormsg = null, $request = null)
     {
         $errormsg = $errormsg == null ? "minimum characters are $rulevalue" : $errormsg;
         if ($rulevalue != null) {
             if (strlen($inputvalue) < $rulevalue) {
                 $this->errors[$inputname][] = $errormsg;
             }
         } else {
             throw new Exception('you must add the number of characters');
         }
     }

     private function max($inputname, $inputvalue, $rulevalue = null, $errormsg = null, $request = null){
         $errormsg = $errormsg == null ? "maximum characters are $rulevalue" : $errormsg;
         if($rulevalue != null) {
             if (strlen( $inputvalue ) > $rulevalue) {
                 $this->errors[$inputname][] = $errormsg;
             }
         } else {
             throw new Exception('you must add the number of characters');
         }
     }

     private function in($inputname, $inputvalue, $rulevalue = null, $errormsg = null, $request = null)
     {
         $errormsg = $errormsg == null ? "you must choose from the listed types only" : $errormsg;
         if ($rulevalue != null) {
             $values = explode(",",$rulevalue);
             if (in_array($inputvalue, $values) == false) {
                 $this->errors[$inputname][] = $errormsg;
             }
         } else {
             throw new Exception('you must indicate the list of values');
         }
     }

}
