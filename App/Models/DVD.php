<?php

namespace App\Models;

class DVD extends Product
{
    public function getProductSpecifications()
    {
        return 'Size : ' . $this->value . ' MB';
    }

    public function formProductSpecifications($request)
    {
        return $request['size'];
    }

    public function addProductProperties()
    {
        $this->properties[] = 'size';
    }
}
