<?php

namespace App\Models;

class Book extends Product
{
    public function getProductSpecifications()
    {
        return 'Weight : ' . $this->value . ' Kg';
    }

    public function formProductSpecifications($request)
    {
        return $request['weight'];
    }

    public function addProductProperties()
    {
        $this->properties[] = 'weight';
    }
}
