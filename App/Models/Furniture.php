<?php

namespace App\Models;

class Furniture extends Product
{
    public function getProductSpecifications()
    {
        return 'Dimensions : ' . $this->value;
    }

    public function formProductSpecifications($request)
    {
        return $request['height'] . 'x' . $request['width'] . 'x' . $request['length'];
    }

    public function addProductProperties()
    {
        $this->properties[] = ['width','length','height'];
    }
}
