<?php

namespace App\Models;

use App\Core\DB;
use PDO;

class ProductsModel
{
    private $id;
    private $sku;
    private $name;
    private $price;
    private $type;
    private $specifications;
    private $db;
    private $table = "products";

    public function __construct()
    {
        $this->db = new DB();
    }

    public function getProducts()
    {
        $stmt = $this->db->connect()->prepare("SELECT * FROM $this->table");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    /**
     * insert new product in db
     * @param array $data => fileds and values of product row
     */
    public function insertProducts()
    {
        $sku = $this->getSku();
        $name = $this->getName();
        $price = $this->getPrice();
        $type = $this->getType();
        $specifications = $this->getSpecifications();

        $stmt = $this->db->connect()->prepare('INSERT INTO '.
                                        $this->table.'(
                                                 sku,
                                                 name,
                                                 price,
                                                 type,
                                                 specifications
                                                 )
                                        VALUES(?, ?, ?, ?,?)');
        return $stmt->execute(array($sku, $name, $price,$type,$specifications));
    }


    /**
     * delete product from db
     * @param int $id => id of product
     */
    public function deleteProduct()
    {
        $id = $this->getId();

        $stmt = $this->db->connect()->prepare("DELETE FROM `$this->table` WHERE `id` = ?");

        $stmt->execute([$id]);
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getSpecifications()
    {
        return $this->specifications;
    }

    public function setSpecifications($specifications)
    {
        $this->specifications = $specifications;
    }
}
