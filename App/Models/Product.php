<?php

namespace App\Models;

abstract class Product
{

    protected $value;
    protected $properties = ['sku','name','price'];

    public function __construct($value = null)
    {
        $this->value = $value;
    }

    abstract protected function getProductSpecifications();

    abstract protected function formProductSpecifications($request);

    abstract protected function addProductProperties();

    public function getProductProperties()
    {
        return $this->properties;
    }
}
