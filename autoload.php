<?php

use App\Core\App;

define("DS",DIRECTORY_SEPARATOR);
define("ROOT_PATH",dirname(__DIR__).DS);
define("APP",'APP'.DS);
define("CORE",APP.'Core'.DS);
define("CONFIG",APP.'Config'.DS);
define("CONTROLLERS",APP.'Controllers'.DS);
define("MODELS",APP.'Models'.DS);
define("VIEWS",APP.'Views'.DS);
define('PUBLIC_PATH', DS."public".DS);
define('CSS_PATH', PUBLIC_PATH  . 'css'.DS);
define('JS_PATH',PUBLIC_PATH . 'js'.DS);

spl_autoload_register(function ($class) {
    $file = str_replace('\\',DIRECTORY_SEPARATOR, $class) . '.php';
    // if the file exists, require it
    if (file_exists($file)) {
        require_once $file;
    }
});

new App();

